O(Objective):
+ In the morning, through Code Review and the teacher's explanation, I gradually became familiar with using Mock for simulation operations.
+ I have learned the basic syntax of SQL and learned how to add and search through SQL statements.
+ I learned how to use JPARepository to control SQL for operations such as adding, deleting, searching, and changing, and applied it to today's exercise project.
+ The Repo is replaced by the JPARepository for Integration testing, and all operations in the service are replaced by the corresponding operations in the JPARepository.

R(Reflective):
+ Fruitful

I(Interpretive):
+ I am not very clear about some of the annotations used in JPA and the role of these annotations.

D(Decision):
+ I will continue to learn the usage methods and logic of these annotations in the future.
