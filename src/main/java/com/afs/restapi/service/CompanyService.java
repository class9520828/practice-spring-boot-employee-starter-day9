package com.afs.restapi.service;

import com.afs.restapi.entity.Company;
import com.afs.restapi.exception.CompanyNotFoundException;
import com.afs.restapi.entity.Employee;
import com.afs.restapi.repository.JPACompanyRepository;
import com.afs.restapi.repository.JPAEmployeeRepository;
import com.afs.restapi.service.dto.CompanyRequest;
import com.afs.restapi.service.dto.CompanyResponse;
import com.afs.restapi.service.dto.EmployeeResponse;
import com.afs.restapi.service.mapper.CompanyMapper;
import com.afs.restapi.service.mapper.EmployeeMapper;
import org.springframework.data.domain.PageRequest;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;

@Service
public class CompanyService {

    private final JPACompanyRepository jpaCompanyRepository;
    private final JPAEmployeeRepository jpaEmployeeRepository;

    public CompanyService(JPACompanyRepository jpaCompanyRepository, JPAEmployeeRepository jpaEmployeeRepository) {
        this.jpaCompanyRepository = jpaCompanyRepository;
        this.jpaEmployeeRepository = jpaEmployeeRepository;
    }

    public List<CompanyResponse> findAll() {
        return CompanyMapper.toResponseList(jpaCompanyRepository.findAll());
    }

    public List<CompanyResponse> findByPage(Integer page, Integer size) {
        return CompanyMapper.toResponseList(jpaCompanyRepository
                .findAll(PageRequest.of(page-1, size))
                .getContent());
    }

    public CompanyResponse findById(Long id) {
        return CompanyMapper.toResponse(jpaCompanyRepository.findById(id).orElseThrow(CompanyNotFoundException::new));
    }

    public void update(Long id, CompanyRequest companyRequest) {
        Optional<Company> optionalCompany = jpaCompanyRepository.findById(id);
        optionalCompany.ifPresent(previousCompany ->
        {
            previousCompany.setName(companyRequest.getName());
            jpaCompanyRepository.save(previousCompany);
        });

    }

    public CompanyResponse create(CompanyRequest companyRequest) {
        Company company = CompanyMapper.toEntity(companyRequest);
        jpaCompanyRepository.save(company);
        return CompanyMapper.toResponse(company);
    }

    public List<EmployeeResponse> findEmployeesByCompanyId(Long id) {

        return jpaCompanyRepository.findById(id).map(company -> EmployeeMapper.toResponseList(company.getEmployees())).orElseThrow(CompanyNotFoundException::new);
    }

    public void delete(Long id) {

        jpaCompanyRepository.deleteById(id);
    }
}
